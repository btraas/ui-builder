# UIBuilder

UIBuilder is a Swift Package for helping with Autolayout

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```

## Usage

```swift
import UIKit

import UIBuilder

override func viewDidLoad() {
    super.viewDidLoad()

    let viewA = UIView()
    let viewB = UIView()
    
    viewA.backgroundColor = .blue
    viewB.backgroundColor = .green

    view.addSubview(viewA)
    view.addSubview(viewB)

    // centres view A in superview (hence optional, can pass in view as well)
    viewA.build()?.center().height(100).width(10).apply()
    // view B top is bottom of viewA, and left matches left of superview
    viewB.build()?.top(viewA, attribute: .bottom).left().height(100).width(50).apply()
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
