//
//  UIView+Visual.swift
//  
//
//  Created by underrateddev on 2020-03-13.
//

import UIKit

public extension UIView {
    
    func round(_ cornerRadius: CGFloat = 4) {
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
        layer.masksToBounds = true
    }
       
    func circular() {
        round(self.bounds.height / 2)
    }
    
    func border(width: CGFloat, color: UIColor) {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
    
    func bottomBorder(color: UIColor) {
        let bottomBorder = CALayer()
        
        bottomBorder.frame = CGRect(x: 0, y: self.frame.height - 1, width: self.frame.width, height: 0.5)
        bottomBorder.backgroundColor = color.cgColor
        
        layer.addSublayer(bottomBorder)
    }
    
}
