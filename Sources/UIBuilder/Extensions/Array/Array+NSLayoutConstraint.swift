//
//  Array+NSLayoutConstraint
//  
//
//  Created by underrateddev on 2020-03-13.
//

import UIKit

public extension Array where Element: NSLayoutConstraint {
    
    func activate() {
        NSLayoutConstraint.activate(self)
    }
    
}
