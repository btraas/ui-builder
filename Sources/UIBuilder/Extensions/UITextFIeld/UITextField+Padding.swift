//
//  UITextField+Padding.swift
//  
//
//  Created by underrateddev on 2020-03-13.
//

import UIKit

public extension UITextField {
    
    func padding(left: CGFloat, right: CGFloat) {
        leftPadding(left)
        leftPadding(right)
    }
    
    func leftPadding(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: frame.size.height))
        
        leftView = paddingView
        leftViewMode = .always
    }
    
    func rightPadding(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: frame.size.height))
        
        rightView = paddingView
        rightViewMode = .always
    }
    
}
