//
//  File.swift
//  
//
//  Created by underrateddev on 2020-03-13.
//

import Foundation

func terminatingFatalError(file: String = #file, function: String = #function, line: Int = #line, additionalInfo: String = "") -> Never {
    fatalError("Terminating:\n\(debugInfo(file: file, function: function, line: line))\n \(additionalInfo)")
}
