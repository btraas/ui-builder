//
//  UIBuilder.swift
//  UI Builder
//
//  Created by underrateddev on 2020-03-06.
//  Copyright © 2020 underrateddev. All rights reserved.
//

import UIKit

// TODO: make config UI Builder that holds UIBuilders in an array or simply constraints
// TODO: validate constraints x anchors & y anchors
// then switch between them easily
// IDEA: maybe create constraint chain builder with .to .offset .multiply etc for more control

/**
 * This class allows you to build constraints for a view
 * It is a more declerative way of doing UI programmatically
 */

public class UIBuilder {
    
    // View that is being anchored
    private let anchoredView: UIView
    
    // Default view in which the anchored view is being constrained
    private let constrainedView: UIView
    
    public private(set) var constraints: [NSLayoutConstraint]
    
    /// Construct a Constraints Builder
    init(anchoredView: UIView, constrainedView: UIView) {
        self.anchoredView = anchoredView
        self.constrainedView = constrainedView
        self.constraints = [NSLayoutConstraint]()
    }
    
    /// Applies the constraints
    public func apply() {
        anchoredView.translatesAutoresizingMaskIntoConstraints = false
        constraints.activate()
    }
    
    // MARK: - Safe Directional Constraints
    /// Creates a safe top anchor constraint
    public func safeTop(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .top, constant: CGFloat = 0) -> UIBuilder {
        return add(safeConstraint(constrainedView, anchoredAttribute: .top, relation: relation, attribute: attribute, constant: constant))
    }
    
    /// Creates a safe bottom anchor constraint
    public func safeBottom(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .bottom, constant: CGFloat = 0) -> UIBuilder {
        return add(safeConstraint(constrainedView, anchoredAttribute: .bottom, relation: relation, attribute: attribute, constant: constant))
    }
    
    /// Creates a safe left anchor constraint
    public func safeLeft(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .left, constant: CGFloat = 0) -> UIBuilder {
        return add(safeConstraint(constrainedView, anchoredAttribute: .left, relation: relation, attribute: attribute, constant: constant))
    }
    
    /// Creates a safe right anchor constraint
    public func safeRight(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .right, constant: CGFloat = 0) -> UIBuilder {
        return add(safeConstraint(constrainedView, anchoredAttribute: .right, relation: relation, attribute: attribute, constant: constant))
    }
    
    // MARK: - Directional Constraints
    /// Creates a top anchor constraint
    public func top(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .top, constant: CGFloat = 0) -> UIBuilder {
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .top, attribute: attribute, constant: constant))
    }
    
    /// Creates a bottom anchor constraint
    public func bottom(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .bottom, constant: CGFloat = 0) -> UIBuilder {
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .bottom, attribute: attribute, constant: constant))
    }
    
    /// Creates a left anchor constraint
    public func left(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .left, constant: CGFloat = 0) -> UIBuilder {
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .left, attribute: attribute, constant: constant))
    }
    
    /// Creates a right anchor constraint
    public func right(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .right, constant: CGFloat = 0) -> UIBuilder {
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .right, attribute: attribute, constant: constant))
    }
    
    // MARK: - Trailing & Leading Constraints
    /// Creates a trailing anchor constraint
    public func trailing(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .trailing, constant: CGFloat = 0) -> UIBuilder {
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .trailing, attribute: attribute, constant: constant))
    }
    
    /// Creates a leading anchor constraint
    public func leading(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .leading, constant: CGFloat = 0) -> UIBuilder {
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .leading, attribute: attribute, constant: constant))
    }
    
    // MARK: - Centering Constraints
    /// Creates a center X anchor constraint
    public func centerX(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .centerX, constant: CGFloat = 0) -> UIBuilder {
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .centerX, attribute: attribute, constant: constant))
    }
    
    /// Creates a center Y anchor constraint
    public func centerY(_ constrainedView: UIView? = nil, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute = .centerY, constant: CGFloat = 0) -> UIBuilder {
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .centerY, attribute: attribute, constant: constant))
    }
    
    /// centers the anchored view inside a constrained view
    public func center(_ constrainedView: UIView? = nil) -> UIBuilder {
        return centerX(constrainedView ?? self.constrainedView).centerY(constrainedView ?? self.constrainedView)
    }
    
    // MARK: - Dimension Constraints
    /// Creates a width anchor constraint with a constant
    public func width(_ constant: CGFloat) -> UIBuilder {
        return add(constraint(attribute: .width, attribute: .notAnAttribute, constant: constant))
    }
    
    /// Creates a height anchor constraint with a constant
    public func height(_ constant: CGFloat) -> UIBuilder {
        return add(constraint(attribute: .height, attribute: .notAnAttribute, constant: constant))
    }
    
    /// Creates a width anchor constraint
    public func width(_ constrainedView: UIView? = nil, attribute: NSLayoutConstraint.Attribute = .width, multipler: CGFloat = 1, constant: CGFloat = 0) -> UIBuilder {
        guard validateDimensions(attribute: attribute) || attribute == .notAnAttribute else {
            terminatingFatalError(additionalInfo: "Attribute can only be .width, .height, or .notAnAttribute, got \(attribute)")
        }
        
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .width, attribute: attribute, multiplier: multipler, constant: constant))
    }
    
    /// Creates a height anchor constraint
    public func height(_ constrainedView: UIView? = nil, attribute: NSLayoutConstraint.Attribute = .height, multipler: CGFloat = 1, constant: CGFloat = 0) -> UIBuilder {
        guard validateDimensions(attribute: attribute) || attribute == .notAnAttribute else {
            terminatingFatalError(additionalInfo: "Attribute can only be .width, .height, or .notAnAttribute, got \(attribute)")
        }
        
        return add(constraint(constrainedView ?? self.constrainedView, attribute: .height, attribute: attribute, multiplier: multipler, constant: constant))
    }

    /// Creates a minimum width anchor constraint with a constant
    public func minimumWidth(_ constant: CGFloat) -> UIBuilder {
        return add(constraint(attribute: .width, relation: .greaterThanOrEqual, attribute: .notAnAttribute, constant: constant))
    }
    
    /// Creates a minimum height anchor constraint with a constant
    public func minimumHeight(_ constant: CGFloat) -> UIBuilder {
        return add(constraint(attribute: .height, relation: .greaterThanOrEqual, attribute: .notAnAttribute, constant: constant))
    }

    /// Creates a maximum width anchor constraint with a constant
    public func maximumWidth(_ constant: CGFloat) -> UIBuilder {
        return add(constraint(attribute: .width, relation: .lessThanOrEqual, attribute: .notAnAttribute, constant: constant))
    }
    
    /// Creates a maximum height anchor constraint with a constant
    public func maximumHeight(_ constant: CGFloat) -> UIBuilder {
        return add(constraint(attribute: .height, relation: .lessThanOrEqual, attribute: .notAnAttribute, constant: constant))
    }
    
    // TODO: check if width or height specified in prev constraints, if standard for aspect ratio use that perhaps?
    /// Creates a constraint for aspect ratio
    public func aspectRatio(_ ratio: CGFloat = 1) -> UIBuilder {
        return width(anchoredView, attribute: .height, multipler: ratio)
    }
    
    // MARK: - Fill Constraints
    /// Fills the anchored view in the constrained view
    public func fill(_ constrainedView: UIView? = nil, constant: CGFloat = 0) -> UIBuilder {
        return top(constrainedView, constant: constant).bottom(constrainedView, constant: -constant).left(constrainedView, constant: constant).right(constrainedView, constant: -constant)
    }
    
    /// Safe fills the anchored view in the constrained view
    public func safeFill(_ constrainedView: UIView? = nil) -> UIBuilder {
        return safeTop(constrainedView).safeBottom(constrainedView).safeLeft(constrainedView).safeRight(constrainedView)
    }
    
}

private extension UIBuilder {
    
    func add(_ constraint: NSLayoutConstraint) -> UIBuilder {
        self.constraints.append(constraint)
        
        return self
    }
    
    func validateDimensions(attribute: NSLayoutConstraint.Attribute) -> Bool {
        return attribute == .width || attribute == .height
    }
    
    func constraint(_ constrainedView: UIView? = nil,
                    attribute anchoredAttribute: NSLayoutConstraint.Attribute,
                    relation: NSLayoutConstraint.Relation = .equal,
                    attribute constrainedAttribute: NSLayoutConstraint.Attribute,
                    multiplier: CGFloat = 1,
                    constant: CGFloat = 0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: anchoredView,
                                  attribute: anchoredAttribute,
                                  relatedBy: relation,
                                  toItem: constrainedView,
                                  attribute: constrainedAttribute,
                                  multiplier: multiplier,
                                  constant: constant)
    }
    
    func safeAnchor<T>(view: UIView, attribute: NSLayoutConstraint.Attribute) -> NSLayoutAnchor<T>? {
        switch attribute {
            case .top:
                return view.safeTopAnchor as? NSLayoutAnchor<T>
            case .bottom:
                return view.safeBottomAnchor as? NSLayoutAnchor<T>
            case .left:
                return view.safeLeftAnchor as? NSLayoutAnchor<T>
            case .right:
                return view.safeRightAnchor as? NSLayoutAnchor<T>
            default:
                terminatingFatalError(additionalInfo: "Cannot get vertical anchor of horizontal attributes")
        }
    }
    
    func safeConstraint<T>(anchoredSafeAnchor: NSLayoutAnchor<T>, constrainedSafeAnchor: NSLayoutAnchor<T>, relation: NSLayoutConstraint.Relation = .equal, constant: CGFloat = 0) -> NSLayoutConstraint {
        switch relation {
            case .equal:
                return anchoredSafeAnchor.constraint(equalTo: constrainedSafeAnchor, constant: constant)
            case .lessThanOrEqual:
                return anchoredSafeAnchor.constraint(lessThanOrEqualTo: constrainedSafeAnchor, constant: constant)
            case.greaterThanOrEqual:
                return anchoredSafeAnchor.constraint(greaterThanOrEqualTo: constrainedSafeAnchor, constant: constant)
            @unknown default:
                terminatingFatalError(additionalInfo: "Unexpected case arrived\(relation)")
        }
    }
    
    func safeConstraint(_ constrainedView: UIView? = nil, anchoredAttribute: NSLayoutConstraint.Attribute, relation: NSLayoutConstraint.Relation = .equal, attribute: NSLayoutConstraint.Attribute, constant: CGFloat = 0) -> NSLayoutConstraint {
        if let anchoredSafeAnchor = safeAnchor(view: anchoredView, attribute: anchoredAttribute) as? NSLayoutXAxisAnchor,
            let constrainedSafeAnchor = safeAnchor(view: constrainedView ?? self.constrainedView, attribute: attribute) as? NSLayoutXAxisAnchor {
            return safeConstraint(anchoredSafeAnchor: anchoredSafeAnchor, constrainedSafeAnchor: constrainedSafeAnchor, relation: relation, constant: constant)
        } else if let anchoredSafeAnchor = safeAnchor(view: anchoredView, attribute: anchoredAttribute) as? NSLayoutYAxisAnchor,
            let constrainedSafeAnchor = safeAnchor(view: constrainedView ?? self.constrainedView, attribute: attribute) as? NSLayoutYAxisAnchor {
            return safeConstraint(anchoredSafeAnchor: anchoredSafeAnchor, constrainedSafeAnchor: constrainedSafeAnchor, relation: relation, constant: constant)
        } else {
            terminatingFatalError(additionalInfo: "Both attributes must be horizontal or vertical attribute types")
        }
    }
    
}
