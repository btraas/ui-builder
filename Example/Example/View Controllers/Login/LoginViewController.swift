//
//  LoginViewController.swift
//  Example
//
//  Created by underrateddev on 2020-03-16.
//  Copyright © 2020 underrateddev. All rights reserved.
//

import UIKit

import UIBuilder

class LoginViewController: UIViewController {
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        
        imageView.backgroundColor = #colorLiteral(red: 0.9629915357, green: 0.9629915357, blue: 0.9629915357, alpha: 1)
        imageView.contentMode = .scaleAspectFit
        if #available(iOS 13.0, *),
            let image = UIImage(systemName: "person.fill") {
            let tintableImage = image.withRenderingMode(.alwaysTemplate).withTintColor(#colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            imageView.image = tintableImage
        }
        
        return imageView
    }()
    let emailTextField: UITextField = {
        let textField = UITextField()
        
        textField.placeholder = "username"
        textField.keyboardType = .emailAddress
        textField.round(16)
        textField.border(width: 2, color: #colorLiteral(red: 0.9629915357, green: 0.9629915357, blue: 0.9629915357, alpha: 1))
        if #available(iOS 13.0, *),
            let image = UIImage(systemName: "person.crop.circle") {
            let tintableImage = image.withRenderingMode(.alwaysTemplate).withTintColor(#colorLiteral(red: 0.9629915357, green: 0.9629915357, blue: 0.9629915357, alpha: 1)).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            textField.icon(tintableImage)
        }
        
        return textField
    }()
    let passwordTextField: UITextField = {
        let textField = UITextField()
        
        textField.placeholder = "password"
        textField.isSecureTextEntry = true
        textField.round(16)
        textField.border(width: 2, color: #colorLiteral(red: 0.9629915357, green: 0.9629915357, blue: 0.9629915357, alpha: 1))
        if #available(iOS 13.0, *),
            let image = UIImage(systemName: "lock") {
            let tintableImage = image.withRenderingMode(.alwaysTemplate).withTintColor(#colorLiteral(red: 0.9629915357, green: 0.9629915357, blue: 0.9629915357, alpha: 1)).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            textField.icon(tintableImage)
        }
        
        return textField
    }()
    let loginButton: UIButton = {
        let button = UIButton(type: .roundedRect)
        
        button.setTitle("Log in", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        button.backgroundColor = #colorLiteral(red: 0.9629915357, green: 0.9629915357, blue: 0.9629915357, alpha: 1)
        button.round(16)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        didLoadSetup()
    }
    
    
    override func viewDidLayoutSubviews() {
//        profileImageView.circular()
        profileImageView.round(16)
    }
    
}

// IDEA: make builder that applies same constraints to any view that is passed in a func
// but you can change certain values, so same width, height, but different margins for example
private extension LoginViewController {
    
    func didLoadSetup() {
        view.addSubview(profileImageView)
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        
        view.backgroundColor = .systemBlue
        
        profileImageView.build()?.safeTop(constant: 100).centerX().height(200).aspectRatio().apply()
        emailTextField.build(profileImageView).top(attribute: .bottom, constant: 40).left().right().height(50).apply()
        passwordTextField.build(emailTextField).top(attribute: .bottom, constant: 20).left().right().height(50).apply()
        loginButton.build(passwordTextField).top(attribute: .bottom, constant: 20).left().right().height(50).apply()
    }
    
    // TODO: make generic function
    func applyGradient() {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [UIColor.black.cgColor, UIColor.darkGray.cgColor, UIColor.gray.cgColor]
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
