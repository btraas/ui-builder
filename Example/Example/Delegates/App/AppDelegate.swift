//
//  AppDelegate.swift
//  Example
//
//  Created by underrateddev on 2020-03-16.
//  Copyright © 2020 underrateddev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setupRootViewController()
        
        return true
    }

}

private extension AppDelegate {
    
    func finishLaunchSetup() {
        setupRootViewController()
    }
    
    func setupRootViewController() {
        // create navigationcontroller
        let navigationController = UINavigationController(rootViewController: MainViewController())
        // let navigationController = UINavigationController(rootViewController: LoginViewController())
        
        // replaces storyboard
        window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
}

